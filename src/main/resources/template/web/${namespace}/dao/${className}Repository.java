<#include "/macro.include"/>
<#include "/java_copyright.include">
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first>
<#assign classNameFirstLower = table.classNameFirstLower> 
package ${basepackage}.${rootbasepackage}.dao;
<#list table.columns as column>
<#if column.pk>
import ${column.javaType};
</#if>
</#list>
import ${basepackage}.domain.${className};

/**
 * <p> Description:${table.tableAlias} Mapper </p>
 * <p> Copyright: Copyright (c) 2019 </p>
 * <p> Create Date: <#if now??>${now?string('yyyy-MM-dd')}</#if></p>
 * <p> Company: YUSYS </p> 
 *@author:it_mck
 *@version:version V1.0
 */
<#list table.columns as column>
<#if column.pk>
public interface ${className}Repository extends JpaRepository<${className},${column.simpleJavaType}>{

}
</#if>
</#list>
