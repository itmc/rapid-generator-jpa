<#include "/macro.include"/>
<#include "/java_copyright.include">
<#assign className = table.className>
<#assign classNameLower = className?uncap_first>
<#assign classNameFirstLower = table.classNameFirstLower>
package ${basepackage}.service;

import java.util.List;
import ${basepackage}.domain.${className};
<#list table.columns as column>
<#if column.pk>
import ${column.javaType};
</#if>
</#list>
/**
 * <p> Description:${table.tableAlias} service </p>
 * <p> Copyright: Copyright (c) 2018 </p>
 * <p> Create by miaoChangKe Date: <#if now??>${now?string('yyyy-MM-dd')}</#if></p>
 *@author:it_mck
 *@version:version V1.0
 */

public class ${className}Service {

	/**
	 * <pre>
	 * <#if now??>${now?string('yyyy-MM-dd')}</#if>
	 * 查询所有${table.tableAlias}数据
	 * </pre>
	 * @return 返回${table.tableAlias}查询结果集合
	 */
	public List<${className}> queryList();
    /**
     * <pre>
     * <#if now??>${now?string('yyyy-MM-dd')}</#if>
     * 普通查询${table.tableAlias}数据
     * </pre>
     */
	<#list table.columns as column>
	<#if column.pk>
    public	${className} selectByPrimaryKey(${column.simpleJavaType} ${column.columnNameLower});
    </#if>
    </#list>
	/**
	 * <pre>
	 * <#if now??>${now?string('yyyy-MM-dd')}</#if>
	 * 新增或修改${table.tableAlias}数据
	 * </pre>
	 * @return 返回${table.tableAlias}查询结果集合
	 */
    public void saveOrUpdate(${className} ${classNameLower}) ;

    /**
     * <pre>
     * <#if now??>${now?string('yyyy-MM-dd')}</#if>
     * 删除${table.tableAlias}数据
     * </pre>
     */
    <#list table.columns as column>
    <#if column.pk>
    public void deleteByPrimaryKey(${column.simpleJavaType} ${column.columnNameLower});
    </#if>
    </#list>

}
