<#include "/macro.include"/>
<#include "/java_copyright.include">
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first>
<#assign classNameFirstLower = table.classNameFirstLower> 
package ${basepackage}.service.impl;
package ${basepackage}.dao.${className}Repository;

import java.util.List;
<#list table.columns as column>
<#if column.pk>
import ${column.javaType};
</#if>
</#list>
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * <p> Description:${table.tableAlias} service </p>
 * <p> Copyright: Copyright (c) 2018 </p>
 * <p> Create by miaoChangKe Date: <#if now??>${now?string('yyyy-MM-dd')}</#if></p>
 *
 *@author:it_mck
 *@version:version V1.0
 */                               
@Service                                            
public class ${className}ServiceImpl implemetes ${className}Service{
	
	@Autowired
    private ${className}Repository ${classNameLower}Repository;
	
	/**
	 * <pre>
	 * <#if now??>${now?string('yyyy-MM-dd')}</#if>
	 * 分页查询${table.tableAlias}数据
	 * </pre>
	 * @return 返回${table.tableAlias}查询结果集合
	 */
	@Override
	public List<${className}> queryList() {
		
        List<${className}> list = ${classNameLower}Repository.findAll();
        return list;
	}

	/**
	 * <pre>
	 * <#if now??>${now?string('yyyy-MM-dd')}</#if>
	 * 新增或修改${table.tableAlias}数据
	 * </pre>
	 * @return 返回${table.tableAlias}查询结果集合
	 */
	@Override
    public void saveOrUpdate(${className} ${classNameLower}) {
		<#list table.columns as column>
	    <#if column.pk>
		if (${classNameLower}.get${column.columnName}() != null) {
    	${className} ${classNameLower}DB = ${classNameLower}Repository.findOne(${classNameLower}.get${column.columnName}());
    	<#else>
    	${classNameLower}DB.set${column.columnName}(${classNameLower}.get${column.columnName}());
	    </#if>
		</#list>
			${classNameLower}Repository.saveAndFlush(${classNameLower}DB);
	     } else {
			${className} ${classNameLower}DB = ${classNameLower}Repository.save(${classNameLower});
	     }
	 }

	/**
	 * <pre>
	 * <#if now??>${now?string('yyyy-MM-dd')}</#if>
	 * 根据主键进行删除${table.tableAlias}数据
	 * </pre>
	 *
 	*/
	@Override
	<#list table.columns as column>
	<#if column.pk>
	 public void deleteByPrimaryKey(${column.simpleJavaType} ${column.columnNameLower}){

		${classNameLower}Repository.delete(${column.columnNameLower});
	}
	</#if>
	</#list>
	
}
