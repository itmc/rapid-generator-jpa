<#include "/macro.include"/>
<#include "/java_copyright.include">
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first> 
package ${basepackage}.domain;

import java.util.Date;
import java.util.List;
<#list table.columns as column>
import ${column.javaType};
</#list>
import javax.persistence.*;


/**
 * <p> Description:${table.tableAlias} domain </p>
 * <p> Copyright: Copyright (c) 2019 </p>
 * <p> Create Date: <#if now??>${now?string('yyyy-MM-dd')}</#if></p>
 * <p> created by miaoChangKe </p>
 *@author:it_mck
 *@version:version V1.0
 */
@Entity
@Table(name = "${table.sqlName}")
public class ${className} {
	
	<#list table.columns as column>
	<#if column.pk>
	
	@Id
    @Column(name = "${column.sqlName}")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	<#else>
	
	@Column(name = "${column.sqlName}")
	</#if>
	private ${column.simpleJavaType} ${column.columnNameLower};//${column.columnAlias} ${column.javaType}
	</#list>

	<#list table.columns as column>
	public void set${column.columnName}(${column.simpleJavaType} value) {
		this.${column.columnNameLower} = value;
	}
	public ${column.simpleJavaType} get${column.columnName}() {
		return this.${column.columnNameLower};
	}
	</#list>
	
	public ${className}(){
	}

	
	public String toString() {
		return new StringBuffer()
		<#list table.columns as column>
			.append("${column.columnName}:["+get${column.columnName}()+"]")
		</#list>
			.toString();
	}

}




