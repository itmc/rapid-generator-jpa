<#include "/macro.include"/>
<#include "/java_copyright.include">
<#assign className = table.className>
<#assign classNameLower = className?uncap_first>
<#assign classNameFirstLower = table.classNameFirstLower>
package ${basepackage}.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.itmck.base.BaseController;
<#list table.columns as column>
<#if column.pk>
import ${column.javaType};
</#if>
</#list>
import ${basepackage}.model.${className};
import ${basepackage}.service.${className}Service;
import org.springframework.web.bind.annotation.RequestBody;
/**
 * <p> Description:${table.tableAlias}信息维护 包括：新增、修改、删除  </p>
 * <p> Copyright: Copyright (c) 2019 </p>
 * <p> Create Date: <#if now??>${now?string('yyyy-MM-dd')}</#if></p>
 *@author:it_mck
 *@version:version V1.0
 */
@RestController
@RequestMapping("/xxx")
public class ${className}Controller extends BaseController{

	@Autowired
    private ${className}Service ${classNameLower}Service;

    /**
	 * <pre>
	 * <#if now??>${now?string('yyyy-MM-dd')}</#if>
	 * 查询所有${table.tableAlias}数据   URL为  /
	 * </pre>
	 * @param ${classNameLower} 参数对象
	 * @return 返回${table.tableAlias}查询结果集合
	 */
    @RequestMapping
    public List<${className}> getPageList() {
        return ${classNameLower}Service.queryList();
    }


    /**
 	 * <pre>
 	 * <#if now??>${now?string('yyyy-MM-dd')}</#if>
 	 * 根据主键查询${table.tableAlias}详情
 	 * </pre>
 	 * @param ${classNameLower} 记录编号
 	 * @return 返回${table.tableAlias}数据对象
 	 */
    <#list table.columns as column>
	<#if column.pk>
	@RequestMapping(value = "/{${column.columnNameLower}}")
	public ${className} getDetail(@PathVariable("${column.columnNameLower}") ${column.simpleJavaType} ${column.columnNameLower}) {
	   return ${classNameLower}Service.selectByPrimaryKey(${column.columnNameLower});
	}
	</#if>
	</#list>

	 /**
 	 * <pre>
 	 * <#if now??>${now?string('yyyy-MM-dd')}</#if>
 	 * 新增或修改${table.tableAlias}记录
 	 * </pre>
 	 * @param ${classNameLower} ${table.tableAlias}对象信息
 	 * @return 返回${table.tableAlias}记录编号
 	 */
    @RequestMapping(value = "/addOrModify")
    public void addOrModify(${className} ${classNameLower}){

		${classNameLower}Service.saveOrUpdate(${classNameLower});
    }

    /**
 	 * <pre>
 	 * <#if now??>${now?string('yyyy-MM-dd')}</#if>
 	 * 删除${table.tableAlias}记录
 	 * </pre>
 	 * @param ${classNameLower} ${table.tableAlias}对象信息
 	 * @return 返回${table.tableAlias}记录编号
 	 */
    <#list table.columns as column>
    <#if column.pk>
	@RequestMapping(value = "/remove/{${column.columnNameLower}}")
    public void remove(@PathVariable("${column.columnNameLower}") ${column.simpleJavaType} ${column.columnNameLower}) {
		${classNameLower}Service.deleteByPrimaryKey(${column.columnNameLower});
	}
    </#if>
	</#list>

}
