package com.itmck.service;

import com.itmck.domain.User;

import java.util.List;

/**
 * Create by it_mck 2019/7/6 0:21
 *
 * @Description:
 * @Version: 1.0
 */
public interface UserService {
    /**
     * 添加
     * @param user
     * @return
     */
    int insertUser(User user);

    /**
     * 删除
     * @param id
     * @return
     */
    void deleteUser(Long id);

    /**
     * 修改
     * @param user
     * @return
     */
    int updateUser(User user);

    /**
     * 查找所有
     * @return
     */
    List<User> selectUsers();
}
