package com.itmck.service.impl;

import com.itmck.dao.UserRepository;
import com.itmck.domain.User;
import com.itmck.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Create by it_mck 2019/7/6 0:22
 *
 * @Description:
 * @Version: 1.0
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public int insertUser(User user) {

        User save = userRepository.save(user);
        if (save != null) {
            return 1;
        }
        return 0;
    }

    @Override
    public void deleteUser(Long id) {

        userRepository.delete(id);
    }

    @Override
    public int updateUser(User user) {
        User user1 = userRepository.saveAndFlush(user);
        if (user1 != null) {
            return 1;
        }
        return 0;
    }


    @Override
    public List<User> selectUsers() {

        List<User> list = userRepository.selectByCondition("");
        return userRepository.findAll();
    }
}
