package com.itmck.web;

import cn.org.rapid_framework.generator.GeneratorFacade;
import cn.org.rapid_framework.generator.GeneratorProperties;

/**
 * Create by it_mck 2019/7/4 20:59
 *
 * @Description:
 * @Version: 1.0
 */
public class GeneratorMain {

    public static void main(String[] args) throws Exception {
        // 模板地址
        String templatePath = "E:\\ideapro\\spring-jpa\\src\\main\\resources\\template";
        GeneratorFacade g = new GeneratorFacade();
        g.getGenerator().addTemplateRootDir(templatePath);
        g.generateByTable("tb_user");   // 通过数据库表生成文件
        g.generateByTable("tb_emp");   // 通过数据库表生成文件
        //g.deleteOutRootDir();  // 删除生成器的输出目录//
        // g.generateByAllTable(); // 自动搜索数据库中的所有表并生成文件,template为模板的根目录
        // g.deleteByTable("table_name", "template");// 按table名字删除文件
        Runtime.getRuntime().exec("cmd.exe /c start " + GeneratorProperties.getRequiredProperty("outRoot"));//打开生成的代码文件夹
    }

}
