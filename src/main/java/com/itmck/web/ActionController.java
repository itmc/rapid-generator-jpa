package com.itmck.web;

import com.itmck.base.BaseController;
import com.itmck.domain.User;
import com.itmck.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Create by it_mck 2019/7/6 0:20
 *
 * @Description:
 * @Version: 1.0
 */
@Controller
@RequestMapping("/stu")
public class ActionController extends BaseController {


    @Autowired
    private UserService userService;


    @RequestMapping("/insert")
    @ResponseBody
    public int insertUser(@RequestBody User user){

        return  userService.insertUser(user);
    }

    @RequestMapping("/delete")
    @ResponseBody
    public void deleteUser(@RequestParam Long id){
        userService.deleteUser(id);
    }


    @RequestMapping("/update")
    @ResponseBody
    public int updateUser(@RequestBody User user){

        int i = userService.updateUser(user);
        return i;
    }


    @RequestMapping("/select")
    @ResponseBody
    public List<User> selectUsers(){

        List<User> list = userService.selectUsers();

        return list;
    }

}
