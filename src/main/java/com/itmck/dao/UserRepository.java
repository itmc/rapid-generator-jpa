package com.itmck.dao;

import com.itmck.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Create by it_mck 2019/7/6 11:50
 *
 * @Description:
 * @Version: 1.0
 */
public interface UserRepository extends JpaRepository<User,Long> {

    @Query(value = "",nativeQuery = true)
    List<User> selectByCondition(String s);
}
